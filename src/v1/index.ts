import { Router } from "express";
import healthz from "./routes/healthz";
import users from "./routes/users";

const router = Router();

router.use("/healthz", healthz);
router.use("/users", users);

export default router;
