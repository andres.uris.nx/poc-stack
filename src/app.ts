import express from "express";

import type { Request, Response, Handler } from "express";

import v1 from "./v1";

// Initialize the express engine
const app = express();

app.use("/v1", v1);

app.use((error: any, req: Request, res: Response, next: Handler) => {
  res.status(error.status || 500);

  return res.json({
    error: {
      message: error.message,
    },
  });
});

export default app;
