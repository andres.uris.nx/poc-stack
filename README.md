# POC-STACK

## Project structure

```
├── jest.config.js
├── package-lock.json
├── package.json
├── prisma
│   ├── migrations
│   │   ├── 20220411175621_init
│   │   │   └── migration.sql
│   │   └── migration_lock.toml
│   ├── schema.prisma
│   └── seed.ts
├── src
│   ├── app.ts
│   ├── server.ts
│   └── v1
│       ├── index.ts
│       └── routes
│           ├── healthz.ts
│           └── users.ts
├── test
│   └── v1
│       └── routes
│           └── healthz.test.ts
└── tsconfig.json
```
