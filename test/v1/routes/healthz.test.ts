import request from "supertest";
import app from "../../../src/app";

describe("Request: GET /v1/healthz", () => {
  test("It should response with 200 status", async () => {
    const response = await request(app).get("/v1/healthz");
    expect(response.statusCode).toBe(200);
  });
});
